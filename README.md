# FPGA视频采集

#### 介绍
本项目采用黑金AX4010，OV5650，AN430，蓝牙HC05实现

#### 功能
1. 通过OV5640采集图像（rgb565），并且在AN430显示
2. 显示模式有图像转灰度图，Sobel检测，腐蚀膨胀
3. 通过蓝牙APP可以选择显示的模式，以及Sobel阈值的动态调整
4. 蓝牙的账号密码是wtu,11801

#### 实验图片
    上面的两张图片
[点击查看效果图片](https://gitee.com/whpUp/fpga-video-capture/blob/master/IMG_20210129_093933.jpg "在这里输入图片标题")


[点击查看app图片](https://gitee.com/whpUp/fpga-video-capture/blob/master/Screenshot_20210131_134621.jpg "在这里输入图片标题")


[点击关注公众号](https://gitee.com/whpUp/fpga-video-capture/blob/master/f.jpg "在这里输入图片标题")