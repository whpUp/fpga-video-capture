



//Sobel 算法，来检测边缘

module Sobel_detection(

		input						clk,
		input						rst,
		
		
		
		input						data_en,
		output					write_en,
			
		
		
		input[7:0]				Sobel_thresh,
		
		
		input[7:0]				data_in,
		output[15:0]			sobel_out
);

wire[7:0]		martix_11;
reg[7:0]			martix_12,martix_13;
wire[7:0]		martix_21;
reg[7:0]			martix_22,martix_23;
reg[7:0]			martix_31,martix_32,martix_33;

reg delay_en1;
reg delay_en2;
reg delay_en3;



assign sobel_out = {Sobel_data,Sobel_data};
assign write_en  = delay_en2;  







//延时两个en周期
always@(posedge clk or negedge rst)
begin

	if(rst == 1'b0)
	begin
		delay_en1 <= 1'b0;
		delay_en2 <= 1'b0;
	end

	else	
	begin
		delay_en1 <= data_en;
		delay_en2 <= delay_en1;
		//delay_en3 <= delay_en2;
	end
end





always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
	begin
		martix_12 <= 'd0;
		martix_13 <= 'd0;	
	end
	
	else if(data_en == 1'b1)
	begin
		martix_12 <= martix_11;
		martix_13 <= martix_12;	
	end
	else
	begin
		martix_12 <= martix_12;
		martix_13 <= martix_13;	
	end
end

always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
	begin
		martix_22 <= 'd0;
		martix_23 <= 'd0;	
	end
	
	else if(data_en == 1'b1)
	begin
		martix_22 <= martix_21;
		martix_23 <= martix_22;	
	end
	else
	begin
		martix_22 <= martix_22;
		martix_23 <= martix_23;	
	end
end

//获取gray数据
always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
	begin
		martix_31 <='d0;
		martix_32 <='d0;
		martix_33 <='d0;
	end
	else if(data_en == 1'b1)
	begin
		martix_31 <= data_in;
		martix_32 <= martix_31;
		martix_33 <= martix_32;
	end
	else
	begin
		martix_31 <= martix_31;
		martix_32 <= martix_32;
		martix_33 <= martix_33;
	end
end




//Sobel 
reg[10:0]	Sobel_px ,Sobel_nx;
reg[10:0]	Sobel_py ,Sobel_ny;

reg[10:0]	Sobel_x;
reg[10:0]	Sobel_y;

wire[7:0]	Sobel_data;


//assign Sobel_x = (Sobel_px > Sobel_nx) ? (Sobel_px - Sobel_nx) : (Sobel_nx - Sobel_px);
//assign Sobel_y = (Sobel_py > Sobel_ny) ? (Sobel_py - Sobel_ny) : (Sobel_ny - Sobel_py);
assign Sobel_data = (Sobel_x + Sobel_y > Sobel_thresh) ? 8'd0 : 8'd255;




always@(posedge clk)
begin
	Sobel_x <= (Sobel_px > Sobel_nx) ? (Sobel_px - Sobel_nx) : (Sobel_nx - Sobel_px);
   Sobel_y <= (Sobel_py > Sobel_ny) ? (Sobel_py - Sobel_ny) : (Sobel_ny - Sobel_py);
end

always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		begin
			Sobel_px <= 'd0;
			Sobel_nx <= 'd0;
		end
	else if(data_en==1'b1)
		begin
			Sobel_nx <= martix_11 + martix_21 + martix_21 + martix_31;
			Sobel_px <= martix_13 + martix_23 + martix_23 + martix_33;
		end
	else
		begin
		Sobel_nx <= 'd0;
		Sobel_px <= 'd0;
		end
end


always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		begin
			Sobel_py <= 'd0;
			Sobel_ny <= 'd0;
		end
	else if(data_en==1'b1)
		begin
			Sobel_py <= martix_11 +martix_12 +martix_12 +  martix_13;
			Sobel_ny <= martix_31 +martix_32 +martix_32 +  martix_33;
		end
	else
		begin
			Sobel_ny <=	'd0;
			Sobel_py <=	'd0;
		end
end
Shift_RAM Shift_RAM(
	.clken(data_en),
	.clock(clk),
	.shiftin(data_in),
	.shiftout(),
	.taps0x(martix_21),
	.taps1x(martix_11));
endmodule 