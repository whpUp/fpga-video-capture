


module ov5640_8_16bit(
	input				pclk,				//ov5640数据输出时钟
	input				rst,				//复位
	input				href,				//场有效，高电平有效
	input				vref,
	input[7:0]		cmos_db,			//ov5640输入的数据
	
	output reg[15:0]	cmos_16db,			//将ov5640整合为16bit的数据输出
	output	reg			data_valid			//数据是否有效
);
reg[7:0]			reg_cmos_16db;

reg[3:0]			delay_10_V;


reg v_d0;
reg v_d1;
wire v_pos;

assign v_pos = (v_d1) & (~v_d0);

always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		begin
			v_d0 <= 'd0;
			v_d1 <= 'd0;
		end
	else
		begin
			v_d0 <= vref;
			v_d1 <= v_d0;
		end
end



always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		delay_10_V <= 'd0;
	else if(delay_10_V >= 'd13)
		delay_10_V <= delay_10_V;
	else if(vref == 1'b1)
		delay_10_V <= delay_10_V + 1'b1;
		

end



always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		reg_cmos_16db <= 'd0;
	else if(href == 1'b1)
		reg_cmos_16db <= cmos_db;

end


reg [8:0]     cnt;


always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		data_valid <= 1'b0;
		
	else if(href == 1'b1)
		if(cnt && cnt[0] == 1'b0 && delay_10_V >= 'd13)
			data_valid <= 1'b1;
		else
			data_valid <= 1'b0;
	else
		data_valid <= 1'b0;

end



always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		cnt <= 'd0;
	else if(href == 1'b1)
		cnt <= cnt + 1'b1;
	else if(href == 1'b0)
		cnt <= 'd0;
	else
		cnt <= cnt;
end


always@(posedge pclk or negedge rst)
begin
	if(rst == 1'b0)
		cmos_16db <= 'd0;
	else if(href == 1'b1)
		cmos_16db <= {reg_cmos_16db,cmos_db};
	else
		cmos_16db <= 16'hffff;
end


endmodule