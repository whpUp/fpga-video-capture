
module sdram_write(
	
		input				clk,
		input				rst,
		
		input				write_en,
		output				write_valid,
		output				flag_write_end,
		
		output			write_valid_test,
		
		output  reg[12:0]	write_addr,
		output	reg[3:0]	write_cmd,
		
		input[12:0]			write_row,
		input[8:0]			write_col,
		
		input[7:0]			wirte_size
);



localparam	NOP			=		4'b0111;
localparam	PRE			=		4'b0010;
localparam	AREF		=		4'b0001;
localparam	WRITE		=		4'b0100;
localparam	ACTIVE		=		4'b0011;


reg[5:0]		write_cnt;




always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		write_cnt <= 'd0;
	else if(flag_write_end == 1'b1)
		write_cnt <= 'd0;
	else if(write_en == 1'b1)
		write_cnt <= write_cnt + 1'b1;
end


always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		write_cmd <= 'd0;
	else if(write_en == 1'b1)
		if(write_cnt == 'd1)
			write_cmd <= ACTIVE;
		else if(write_cnt >= 'd4 && write_cnt <= 'd3 + wirte_size)
			write_cmd <= WRITE;
		else if(write_cnt == 'd5 + wirte_size)
			write_cmd <= PRE;
		else
			write_cmd <= NOP;
	else
		write_cmd <= NOP;
end

always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		write_addr <= 'd0;
	else if(write_en == 1'b1)
		if(write_cnt == 'd1)
			write_addr <= write_row;
		else 
			write_addr <= {4'b0000,write_col+write_cnt -	'd4};	
end


assign flag_write_end = (write_cnt >= 'd7 + wirte_size) ? 1'b1 : 1'b0;
assign write_valid    = (write_cnt >= 'd5 && write_cnt <= 'd4 + wirte_size) ? 1'b1 : 1'b0;
assign write_valid_test = (write_cnt >= 'd4 && write_cnt <= 'd3 + wirte_size) ? 1'b1 : 1'b0;


endmodule 