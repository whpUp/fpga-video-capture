
module sdram_read(
	
		input				clk,
		input				rst,
		
		input				read_en,
		output				read_valid,
		output				flag_read_end,
		
		output	[15:0]		read_data,
		output  reg[12:0]	read_addr,
		output	reg[3:0]	read_cmd,
		
		input[12:0]			read_row,
		input[8:0]			read_col
);

localparam	NOP			=		4'b0111;
localparam	PRE			=		4'b0010;
localparam	AREF		=		4'b0001;
localparam	READ		=		4'b0101;
localparam	ACTIVE		=		4'b0011;




reg[5:0]	read_cnt;




always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		read_cnt <= 'd0;
	else if(flag_read_end == 1'b1)
		read_cnt <= 'd0;
	else if(read_en == 1'b1)
		read_cnt <= read_cnt + 1'b1;
end


always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		read_cmd <= 'd0;
	else if(read_en == 1'b1)
		if(read_cnt == 'd1)
			read_cmd <= ACTIVE;
		else if(read_cnt >= 'd4 && read_cnt <= 'd43)
			read_cmd <= READ;
		else if(read_cnt == 'd46)
			read_cmd <= PRE;
		else
			read_cmd <= NOP;
	else
		read_cmd <= NOP;
end


always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		read_addr <= 'd0;
	else if(read_en == 1'b1)
		if(read_cnt == 'd1)
			read_addr <= read_row;
		else
			read_addr <= {4'b0000,read_col + read_cnt - 'd4};
		
end



assign flag_read_end = (read_cnt >= 'd48) ? 1'b1 : 1'b0;
assign read_valid    = (read_cnt >= 'd8 && read_cnt <= 'd47) ? 1'b1 : 1'b0;





endmodule 