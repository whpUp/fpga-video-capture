
module sdram_aref(
	
	input			clk,
	input			rst,
	
	
	input					aref_en,
	output	wire			aref_req,
	output	wire			aref_end,
	
	
	output reg[3:0]			aref_cmd,
	output wire[12:0]		aref_addr,

	input					flag_init_end
);
localparam	NOP			=		4'b0111;
localparam	PRE			=		4'b0010;
localparam	AREF		=		4'b0001;

//?750us????
localparam	DELAY_15US		=		720;

reg[4:0]		cmd_cnt;
reg[9:0]		aref_cnt;


//?????
always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		aref_cnt <= 'd0;
	else if(aref_end == 1'b1)
		aref_cnt <= 'd0;
	else if(flag_init_end == 1'b1)
		aref_cnt <= aref_cnt + 1'b1;
end

//?????
always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)
		cmd_cnt <= 'd0;
	else if(aref_end == 1'b1)
		cmd_cnt <= 1'b0;
	else if(aref_en == 1'b1)
		cmd_cnt <= cmd_cnt + 1'b1;
end

//??
always@(posedge clk or negedge rst)
begin
	if(rst == 1'b0)	
		aref_cmd <= 'd0;
	else if(aref_en == 1'b1)
		case(cmd_cnt)
		0:			aref_cmd <= PRE;
		2:			aref_cmd <= AREF;
		10:			aref_cmd <= AREF;
		default:	aref_cmd <= NOP;
		endcase
	else
		aref_cmd <= NOP;
end





assign 	aref_req 	= 	(aref_cnt >= DELAY_15US) ? 1'b1 : 1'b0;		//????
assign	aref_end 	= 	(cmd_cnt >= 'd16) ? 1'b1 : 1'b0;				//????
assign 	aref_addr 	= 	13'b0_0100_0000_0000;
	


endmodule	