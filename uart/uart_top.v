
//每接收到两个字符就产生一个应答信号，输出数据为16位


module uart_top_an430(
	input					clk_50M,				//时钟线
	input					rst,					//复位信号
	
	input					uart_rx,				//uart接收信号
	
	output reg			uart_rx_ack,		//uart应答
	output[15:0]		uart_data_out     //接收到的信号
);



wire			recv_ack;			//接收到一个字节的应答
wire[7:0]	recv_data;			//接收到一个字节的数据


reg[15:0]	out_data;
reg		   data_cnt;

assign uart_data_out = out_data;


//assign uart_rx_ack = (recv_ack == 1'b1 && data_cnt == 1'b1) ? 1'b1 : 1'b0;

always@(posedge clk_50M or negedge rst)
begin
	if(rst == 1'b0)
		uart_rx_ack <=1'b0;
	else if(recv_ack == 1'b1 && data_cnt == 1'b1)
		uart_rx_ack <= 1'b1;
	else
		uart_rx_ack <= 1'b0;
		

end

always@(posedge clk_50M or negedge rst)
begin
	if(rst == 1'b0)
		data_cnt <= 1'b0;
	else if(recv_ack == 1'b1)
		if(data_cnt == 1'b1)
			data_cnt <= 1'b0;
		else
			data_cnt <= 1'b1;
	else
		data_cnt <= data_cnt;
end
always@(posedge clk_50M or negedge rst)
begin
	if(rst == 1'b0)
		out_data <= 'd0;
	else if(recv_ack == 1'b1)
			out_data <= {out_data[7:0],recv_data};
	
	else
		out_data <= out_data;
end





uart_rx uart_rx_m0
(
	.clk(clk_50M),              //clock input
	.rst_n(rst),            //asynchronous reset input, low active 
	.rx_data(recv_data),          //received serial data
	.rx_data_valid(recv_ack),    //received serial data is valid
	.rx_data_ready(1'b1),    //data receiver module ready
	.rx_pin(uart_rx)            //serial data input
);

endmodule 
